# Ambisonics Web Player

## MQTT Topics To Control Player

- `test`, `KOFFER/test`, `CLIENTID/test` Prints message and client id
- `CLIENTID/koffer` (Websocket or MQTT) To change Koffer listend to
- `CLIENTID/zone` (Websocket or MQTT) Changes zone variable and switches tracks if player is playing
- `CLIENTID/systemsound` (Websocket or MQTT) Plays stereo sounds. Supported messages for volume: _UP_ and _DOWN_ 
- `CLIENTID/headtracker` (Websocket or MQTT) Rotate ambix player scene. Supported message has array [roll, pitch, yaw] in °
- `KOFFER/play` Play ambix. Message can be integer as offset in seconds
- `KOFFER/setplay` Sets tracklist and starts playback based on current zone
- `KOFFER/pause` 
- `KOFFER/stop` 
- `KOFFER/time` Jump to time in current audio source in seconds 
- `KOFFER/track` Message must be key set in tracklist
- `KOFFER/tracklist` Message must be tracklist object with key and path to audio source
- `KOFFER/volume` Message must be integer


## Getting started on Raspberry Pi

- Follow [Headless Image Setup](https://gitlab.com/machina_ex/sign_here/barcode-reader#headless-image-setup) and [Image Setup](https://gitlab.com/machina_ex/sign_here/barcode-reader#image-setup) but with `ssid="meX"` in _boot/wpa_supplicant.conf_
- Change hostname to `playerNUMBEROFBOARD`
- Enabel SSH and set WLAN localization to DE
- Install _Rclone_, _Screen_, _Chrome_, _Pip3_ and _Git_ on the Pi with (`sudo`) `apt install git rclone screen chromium-browser python3-pip -y`
- Install Node.js on your Pi
  with _nvm_:  
    - `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash`  
    - Restart yout Terminal and install Node.js `nvm install node`  

  from _unofficial build_  
    - `wget <link to NodeJS ARMv61 from nodejs.org>` ([link](https://unofficial-builds.nodejs.org/download/release/))
    - `tar xvfJ <file.tar.xz>`
    - `sudo cp -R <extracted tar folder>/* /usr/local`
    - (`rm -rf node-*`)
    - `sudo reboot`
- Download this repository with `git clone https://gitlab.com/machina_ex/layers_of_life/player.git`
- Install the project with `npm install`
- Setup _rclone_ with  `mkdir ~/.config`, `mkdir ~/.config/rclone` and `nano ~/.config/rclone/rclone.conf`
```
    [meXcloud]
    type = webdav
    url = https://cloud.machinaex.org/remote.php/dav/files/playersoflife/
    vendor = nextcloud
    user = playersoflife
    pass = PASSWORDINKEYPASS
```
  - Copy in `player` folder with `rclone -v copy meXcloud:Layers/players client/assets`

- (Point Puppeteer to Chromium in _launch()_ e.g.: `executablePath: '/usr/bin/chromium-browser'`)
- Start the Player with `npm start` 

- Install `pip3 install websockets`

  


