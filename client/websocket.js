function openWebsocket({ host = "localhost", port = 8967, subscriptions = [] } = {}) {
    let socket = new WebSocket(`ws://${host}:${port}`);

    socket.onopen = function (e) {
        logInfo("WEBSOCKET openConnection established");
    };

    socket.onmessage = function (event) {
        // logInfo(`WEBSOCKET message Data received from server: ${event.data}`);
        const msgObj = JSON.parse(event.data);
        const topic = msgObj?.topic;
        const message = msgObj?.message;
        subscriptions.forEach((s) => {
            try {
                if (topicsMatch(topic, s.topics)) s.callback(topic, message);
                return true;
            } catch (error) {
                logError(error);
                return false;
            }
        });
    };

    socket.onclose = function (event) {
        logInfo('WEBSOCKET close. Reconnect will be attempted in 1 second.');
        setTimeout(() => openWebsocket({ host, port, subscriptions }), 5000);
    };

    socket.onerror = function (error) {
        logInfo(`WEBSOCKET error`, error);
    };

    const topicsMatch = (topicIn, topicsSub) => {
        for (let index = 0; index < topicsSub.length; index++) {
            const t = topicsSub[index];
            const res = mqttWildcard(topicIn, t);
            if (res && res.length > 0) return true;
        }
        return false;
    };

    // Stolen from hobbyquaker https://github.com/hobbyquaker/mqtt-wildcard
    const mqttWildcard = (topic, wildcard) => {
        if (topic === wildcard || wildcard === "#") {
            return [topic];
        }
        const res = [];
        const t = String(topic).split("/");
        const w = String(wildcard).split("/");
        let i = 0;
        for (const lt = t.length; i < lt; i++) {
            if (w[i] === "+") {
                res.push(t[i]);
            } else if (w[i] === "#") {
                res.push(t.slice(i).join("/"));
                return res;
            } else if (w[i] !== t[i]) {
                return null;
            }
        }
        if (w[i] === "#") {
            i += 1;
        }
        return i === w.length ? res : null;
    };

    socket.publish = function (topic, data){
        this.send(JSON.stringify({ topic: topic, message: data}));
    };

    return socket;
}
