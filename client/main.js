const LOGLEVEL = "PRODUCTION"; // 'PRODUCTION'

// TODO load json file scene list
// const SCENELIST = 'assets/audio/SCENELIST.json';
let GLOBAL_KOFFER = getParameter("koffer");
GLOBAL_KOFFER = GLOBAL_KOFFER == 'undefined' ? 'koffer1' : GLOBAL_KOFFER;
let ZONE = "MITTE";

const BASEPATH = "assets/audio/";
const SWOOSH = "SWOOSH_KURZ_ST.ogg";
const VOLUP = "VOL_UP_ST.ogg";
const VOLDOWN = "VOL_DOWN_ST.ogg";

const CLIENTID = getParameter("host");
const BROKER = "192.168.4.1"; // koffer1.local koffer1

const CONTEXT = new AudioContext();
const ambixPlayer = new AmbixPlayer(CONTEXT, BASEPATH);
const stereoPlayer = new AudioPlayer(CONTEXT, BASEPATH);
const systemPlayer = new AudioPlayer(CONTEXT, BASEPATH);
let activePlayer = stereoPlayer;
let connectionTimer;

const SUBSCRIPTIONS = ({ KOFFER = "koffer1", CLIENTID = randomString("p_"), } = {}) => [
    {
        topics: [`${KOFFER}/time/counter`],
        callback: (topic, data) => {
            logInfo(`got heartbeat from ${KOFFER}`);
            clearTimeout(connectionTimer);
            connectionTimer = setTimeout(()=>
            {
                logInfo('lost connection to MQTT Broker. reconnect');
                MQTT_CONNECTION.end();
                MQTT_CONNECTION = MQTT({
                    host: BROKER,
                    clientId: CLIENTID,
                    subscriptions: SUBSCRIPTIONS({ KOFFER: GLOBAL_KOFFER, CLIENTID }),
                });
            }, 15000);
        },
    },
    {
        topics: [`test`, `${CLIENTID}/test`, `${KOFFER}/test`],
        callback: (topic, data) => logInfo("TEST:", CLIENTID, topic, data),
    },
    {
        topics: [`${CLIENTID}/koffer`, `set/${CLIENTID}/koffer`],
        callback: (topic, k) => {
            if (GLOBAL_KOFFER === k.toString()) {
                //nothing todo, otherwise there is a loop when using a retained topic
                return;
            }
            GLOBAL_KOFFER = k.toString();
            logInfo(`inside change koffer with ${GLOBAL_KOFFER} as new GLOBAL_KOFFER`)
            MQTT_CONNECTION.end();
            MQTT_CONNECTION = MQTT({
                host: BROKER,
                clientId: CLIENTID,
                subscriptions: SUBSCRIPTIONS({ KOFFER: GLOBAL_KOFFER, CLIENTID }),
            });
            MQTT_CONNECTION.publish(`dev/${CLIENTID}/koffer`, JSON.stringify({ koffer: GLOBAL_KOFFER }));
        },
    },
    {
        topics: [`${CLIENTID}/zone`, `set/${CLIENTID}/zone`], // on zone change
        callback: (topic, ir) => {
            let newZone = "";
            switch (isInteger(ir, ir.toString())) {
                case 13163:
                case "MITTE":
                    newZone = "MITTE";
                    break;
                case 14819:
                case 15356:
                case "AUSSEN":
                    newZone = "AUSSEN";
                    break;
                default:
                    newZone = ZONE;
            }
            logInfo(`inside zone callback with ${ir} as newZone = ${newZone} and old ZONE = ${ZONE}`);
            if (newZone != ZONE) {
                ZONE = newZone;
                MQTT_CONNECTION.publish(`dev/${CLIENTID}/zone`, JSON.stringify({ zone: ZONE }));
                systemPlayer.stop();
                systemPlayer.src = SWOOSH;
                if (activePlayer.currentTime != 0) {
                    const offset = activePlayer.currentTime; // works only if zones have same audio length
                    activePlayer.pause();
                    systemPlayer.play().then(() => {
                        activePlayer.track = ZONE; 
                        activePlayer.play(offset).then(
                            () => 
                            {
                                console.log(`${KOFFER}/onplayed`, activePlayer.tracklist["STATE"], activePlayer);
                                MQTT_CONNECTION.publish(`${KOFFER}/onplayed`, activePlayer.tracklist["STATE"])
                            },
                            () => MQTT_CONNECTION.publish(`${KOFFER}/onerror`, `file not found ${activePlayer.tracklist[ZONE]}`)
                        );
                    }, 
                        logError);
                } else {
                    systemPlayer.play();
                }
            }
        },
    },
    {
        topics: [`${CLIENTID}/systemsound`],
        callback: (topic, key) => {
            logInfo(`inside system sound with ${key}`)
            MQTT_CONNECTION.publish(`dev/${CLIENTID}/systemsound`, String(key));
            switch (key) {
                case "UP":
                    systemPlayer.src = VOLUP;
                    break;
                case "DOWN":
                    systemPlayer.src = VOLDOWN;
                    break;
                default:
                    systemPlayer.src = undefined;
                    break;
            }
            systemPlayer.play();
        },
    },
    {
        topics: [`${CLIENTID}/battery`],
        callback: (topic, key) => {
            const batteryMsg = JSON.stringify(key)
            logInfo(`inside battery with ${batteryMsg}`)
            MQTT_CONNECTION.publish(`dev/${CLIENTID}/battery`, batteryMsg);
        },
    },
    {
        topics: [`${CLIENTID}/headtracker`],
        callback: (topic, data) => ambixPlayer.rotate(JSON.parse(data)),
    },
    {
        topics: [`${KOFFER}/play`],
        callback: (topic, data) =>
            activePlayer.play(isInteger(data, false)).then(
                () => MQTT_CONNECTION.publish(`${KOFFER}/onplayed`, activePlayer?.["STATE"]),
                () => MQTT_CONNECTION.publish(`${KOFFER}/onerror`, `file not found`)
            ),
    },
    {
        topics: [`${KOFFER}/setplay`],
        callback: (topic, scene) => {
            const _tracklist = JSON.parse(scene);
            activePlayer.stop();
            activePlayer = _tracklist.PLAYER == "AX" ? ambixPlayer : stereoPlayer;
            activePlayer.tracklist = _tracklist;
            activePlayer.src = _tracklist[ZONE];
            const OFFSET = isInteger(_tracklist?.["OFFSET"], 0)
            activePlayer.play(OFFSET).then(
                () => MQTT_CONNECTION.publish(`${KOFFER}/onplayed`, _tracklist?.["STATE"]),
                () => MQTT_CONNECTION.publish(`${KOFFER}/onerror`, `file not found ${_tracklist[ZONE]}`)
            );
            activePlayer.getDuration().then((duration) =>
                MQTT_CONNECTION.publish(`dev/${CLIENTID}/last_play`,
                    JSON.stringify({ start: counter - OFFSET, file: _tracklist[ZONE], duration: duration })
                )
            );
        },
    },
    {
        topics: [`${KOFFER}/pause`],
        callback: (topic, data) => activePlayer.pause(),
    },
    {
        topics: [`${KOFFER}/stop`],
        callback: (topic, data) => activePlayer.stop(),
    },
    {
        topics: [`${KOFFER}/time`],
        callback: (topic, data) => (activePlayer.currentTime = isInteger(data)),
    },
    {
        topics: [`${KOFFER}/track`],
        callback: (topic, key) => (activePlayer.track = key.toString()),
    },
    {
        topics: [`${KOFFER}/tracklist`],
        callback: (topic, data) => {
            const _tracklist = JSON.parse(scene);
            activePlayer.stop();
            activePlayer = _tracklist.PLAYER == "ST" ? stereoPlayer : ambixPlayer;
            activePlayer.tracklist = _tracklist;
        },
    },
    {
        topics: [`${KOFFER}/volume`],
        callback: (topic, data) => {
            ambixPlayer.volume = Number(data);
            stereoPlayer.volume = Number(data);
        },
    },
    {
        topics: [`get/${CLIENTID}/koffer`],
        callback: (topic, data) => MQTT_CONNECTION.publish(`dev/${CLIENTID}/koffer`, JSON.stringify({ koffer: GLOBAL_KOFFER })),
    },
    {
        topics: [`get/${CLIENTID}/zone`],
        callback: (topic, data) => MQTT_CONNECTION.publish(`dev/${CLIENTID}/zone`, JSON.stringify({ zone: ZONE })),
    },
    {
        topics: [`set/${CLIENTID}/shutdown`],
        callback: (topic, data) => {
            MQTT_CONNECTION.publish(`dev/${CLIENTID}/shutdown`, data.toString());
            WS_CLIENT.publish(`${CLIENTID}/shutdown`, data.toString());
        },
    },
];

let MQTT_CONNECTION = MQTT({
    host: BROKER,
    clientId: CLIENTID,
    subscriptions: SUBSCRIPTIONS({ KOFFER: GLOBAL_KOFFER, CLIENTID }),
});

MQTT_CONNECTION.publish(`dev/${CLIENTID}/koffer`, JSON.stringify({ koffer: GLOBAL_KOFFER }));
MQTT_CONNECTION.publish(`dev/${CLIENTID}/zone`, JSON.stringify({ zone: ZONE }));

const WS_CLIENT = openWebsocket({
    host: 'localhost',
    subscriptions: SUBSCRIPTIONS({ KOFFER: GLOBAL_KOFFER, CLIENTID }),
});

const manualRotate = () => localRotate(ambixPlayer);

let counter = 0;
let interval = setInterval(function () {
    MQTT_CONNECTION.publish(`dev/${CLIENTID}/heartbeat`, String(counter));
    counter++;
}, 1000);

