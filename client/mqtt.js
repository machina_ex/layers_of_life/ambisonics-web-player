function MQTT({
    host = "localhost",
    port = 9001,
    clientId = randomString("js_"),
    subscriptions = [],
    pass = "",
    user = "",
} = {}) {
    const client = mqtt.connect(`ws://${host}:${port}`, {
        clientId,
        pass,
        user,
        reconnectPeriod: 5 * 1000,
        connectTimeout: 15 * 1000,
        resubscribe: true,
    });

    client.on("connect", function () {
        logInfo(`successfully connected to broker ${host} as ${clientId}`);
        subscriptions.forEach((s) => {
            client.subscribe(s.topics, (err) => {
                if (err) logError(err);
            });
        });
    });

    client.on("message", (topic, message) => {
        logInfo(`MQTT: got message ${topic} ${message}`);
        subscriptions.forEach((s) => {
            try {
                if (topicsMatch(topic, s.topics)) s.callback(topic, message.toString());
                return true;
            } catch (error) {
                logError(error);
                return false;
            }
        });
    });

    client.on("error", (error) => logError("MQTT", error));
    client.on("reconnect", (packet) => logInfo("MQTT reconnect"));
    client.on("close", (packet) => logInfo("MQTT close"));
    client.on("disconnect", (packet) =>
        logInfo("disconnect", JSON.stringify(packet))
    );
    client.on("offline", (packet) => logInfo("MQTT offline"));
    client.on("end", (packet) => logInfo("MQTT end"));

    const topicsMatch = (topicIn, topicsSub) => {
        for (let index = 0; index < topicsSub.length; index++) {
            const t = topicsSub[index];
            const res = mqttWildcard(topicIn, t);
            if (res && res.length > 0) return true;
        }
        return false;
    };

    // Stolen from hobbyquaker https://github.com/hobbyquaker/mqtt-wildcard
    const mqttWildcard = (topic, wildcard) => {
        if (topic === wildcard || wildcard === "#") {
            return [topic];
        }
        const res = [];
        const t = String(topic).split("/");
        const w = String(wildcard).split("/");
        let i = 0;
        for (const lt = t.length; i < lt; i++) {
            if (w[i] === "+") {
                res.push(t[i]);
            } else if (w[i] === "#") {
                res.push(t.slice(i).join("/"));
                return res;
            } else if (w[i] !== t[i]) {
                return null;
            }
        }
        if (w[i] === "#") {
            i += 1;
        }
        return i === w.length ? res : null;
    };

    return client
}
